from sqlalchemy import Column, Integer, String, Boolean, Table, ForeignKey
from sqlalchemy.orm import relationship, validates
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

association_table = Table('registration_sectors', Base.metadata,
                          Column('sector_id', Integer,
                                 ForeignKey('sectors.id')),
                          Column('registration_id', Integer,
                                 ForeignKey('registrations.id'))
                          )


class Sector(Base):
    __tablename__ = 'sectors'

    id = Column(Integer, primary_key=True)
    name = Column(String)
    parent_id = Column(Integer)
    registrations = relationship(
        "Registration",
        secondary=association_table,
        back_populates="sectors")

    def __repr__(self):
        return "<Sector(name='%s', parent_id='%s')>" % (
            self.name, self.parent_id)


class Registration(Base):
    __tablename__ = 'registrations'

    @validates('name', 'agreed', 'sectors')
    def validate_existence(self, key, value):
        assert value != '', f"Field missing: {key}"
        return value

    id = Column(Integer, primary_key=True)
    name = Column(String)
    agreed = Column(Boolean)
    sectors = relationship(
        "Sector",
        secondary=association_table,
        back_populates="registrations")

    def __repr__(self):
        return "<Registration(name='%s', agreed='%s')>" % (
            self.name, self.agreed)
