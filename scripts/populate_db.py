import codecs
from os.path import dirname, abspath
import os
import sys
from bs4 import BeautifulSoup
import mysql.connector as mariadb


dir_path = dirname(dirname(abspath(__file__)))
html_doc = open(dir_path + "/assets/TEST.html", "r")
soup = BeautifulSoup(html_doc, "html.parser")

select = soup.find("select")
options = select.findChildren("option")

sectors = []
for option in options:
    sector = {}

    text = option.getText()
    sector["text"] = text.replace("\xa0", "")
    sector["id"] = option["value"]
    sector["depth"] = text.count("\xa0") // 4
    sectors.append(sector)

sectors_enhanced = []
for idx, sector in enumerate(sectors):
    preceding_items_reversed = list(reversed(sectors[:idx]))
    for preceding_item in preceding_items_reversed:
        if preceding_item["depth"] < sector["depth"]:
            sector["parent_id"] = preceding_item["id"]
            break

    sectors_enhanced.append(sector)


def est_db_conn():
    # DRY and shift this into a module
    try:
        db_user = os.environ["DB_USER"]
        db_pass = os.environ["DB_PASSWORD"]
        db_host = os.environ["DB_HOST"]
        db_port = os.environ["DB_PORT"]
        db_name = os.environ["DB_NAME"]

        mariadb_connection = mariadb.connect(
            user=db_user, password=db_pass, database=db_name, host=db_host, port=db_port)
        mariadb_connection.autocommit = True
        return mariadb_connection
    except mariadb.Error as e:
        print(f"Error connecting to MariaDB Platform: {e}")
        sys.exit(1)


def create_sectors_table(cur):
    try:
        cur.execute(
            "CREATE TABLE IF NOT EXISTS sectors (`id` INT NOT NULL, `name` VARCHAR(255) NOT NULL, `parent_id` INT, PRIMARY KEY (`id`))")
    except mariadb.Error as e:
        print(f"MariaDB Error: {e}")
        sys.exit(1)


def create_registrations_table(cur):
    try:
        cur.execute(
            "CREATE TABLE IF NOT EXISTS registrations (`id` INT NOT NULL AUTO_INCREMENT, `name` VARCHAR(255) NOT NULL, `agreed` BOOL NOT NULL, PRIMARY KEY (`id`))")
    except mariadb.Error as e:
        print(f"MariaDB Error: {e}")
        sys.exit(1)


def create_registration_sectors_table(cur):
    try:
        cur.execute(
            "CREATE TABLE IF NOT EXISTS registration_sectors (`id` INT NOT NULL AUTO_INCREMENT, `sector_id` INT NOT NULL, `registration_id` INT NOT NULL, PRIMARY KEY (`id`))")
    except mariadb.Error as e:
        print(f"MariaDB Error: {e}")
        sys.exit(1)


def populate_db_with_sectors(cur, sectors):
    try:
        for sector in sectors:
            if "parent_id" not in sector:
                sector["parent_id"] = None
            cur.execute("INSERT INTO sectors (id, name, parent_id) VALUES (%s, %s, %s)",
                        (sector["id"], sector["text"], sector["parent_id"]))
    except mariadb.Error as e:
        print(f"MariaDB Error: {e}")
        sys.exit(1)


db_conn = est_db_conn()
cursor = db_conn.cursor()
create_sectors_table(cursor)
create_registrations_table(cursor)
create_registration_sectors_table(cursor)
populate_db_with_sectors(cursor, sectors_enhanced)
cursor.close()
db_conn.close()
