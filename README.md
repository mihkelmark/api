# API
 
## Running in dev mode with docker:
(Requires [Docker](https://docs.docker.com/get-docker/))

1. Clone the repo and cd: `git clone git@bitbucket.org:mihkelmark/api.git && cd api`
2. While in this directory, build the images and containers: `docker-compose up`
3. Then populate the database: `docker-compose run api python scripts/populate_db.py`. This creates the necessary database tables, scrapes the supplied html document for data and inserts it into the database.

With the default configuration the API is served on http://localhost:3009

## Available endpoints:
- GET `/sectors`
- POST `/registrations`