import tornado.ioloop
import tornado.web
import os
import traceback
import json
import sys
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from model import Sector, Registration


def db_session():
    db_user = os.environ["DB_USER"]
    db_pass = os.environ["DB_PASSWORD"]
    db_host = os.environ["DB_HOST"]
    db_port = os.environ["DB_PORT"]
    db_name = os.environ["DB_NAME"]

    try:
        engine = create_engine(
            f'mysql+mysqlconnector://{db_user}:{db_pass}@{db_host}:{db_port}/{db_name}', pool_size=20, max_overflow=0, echo=os.environ.get("ENV") == "dev")
        return sessionmaker(bind=engine)
    except:
        print("Error creating ORM session.")
        sys.exit(1)
        engine.dispose()



class BaseHandler(tornado.web.RequestHandler):
    def set_default_headers(self):
        self.set_header("Content-Type", "application/json")
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Headers", "content-type")
        self.set_header('Access-Control-Allow-Methods',
                        'POST, GET, OPTIONS, PATCH, PUT')

    def options(self):
        self.set_status(204)
        self.finish()

    def write_error(self, status_code, **kwargs):
        if os.environ["ENV"] == "dev" and "exc_info" in kwargs:
            err_lines = []
            for line in traceback.format_exception(*kwargs["exc_info"]):
                err_lines.append(line)
            self.finish({
                "status": "error",
                "code": status_code,
                "message": self._reason,
                "traceback": err_lines
            })
        else:
            # Stay classy in prod
            self.finish({
                "status": "error",
                "message": self._reason
            })


class PingHandler(BaseHandler):
    def get(self):
        self.write({"status": "success", "result": "pong"})


class NotFoundHandler(BaseHandler):
    def prepare(self):
        raise tornado.web.HTTPError(
            status_code=404,
            reason="Resource not found"
        )


class RegistrationsHandler(BaseHandler):
    def initialize(self, db_session):
        self.session = db_session()

    def post(self):
        received_data = json.loads(self.request.body)
        new_reg = Registration(
            name=received_data["name"], agreed=received_data["agreed"])
        self.session.add(new_reg)
        self.session.commit()

        for sector_id in received_data["sectors"]:
            my_sector = self.session.query(Sector).get(sector_id)
            new_reg.sectors.append(my_sector)
            self.session.commit()
        self.session.close()

        self.write(
            {"status": "success", "result": received_data})


class SectorsHandler(BaseHandler):
    def initialize(self, db_session):
        self.session = db_session()

    def get(self):
        sectors = []
        for sector in self.session.query(Sector).order_by(Sector.id):
            sectors.append(dict(id=sector.id, name=sector.name,
                                parent_id=sector.parent_id))
        self.session.close()

        self.write({"status": "success", "result": sectors})


def make_app():
    is_dev_mode = os.environ.get("ENV") == "dev"

    return tornado.web.Application([
        (r"/ping", PingHandler),
        (r"/sectors", SectorsHandler, dict(db_session=db_session())),
        (r"/registrations", RegistrationsHandler, dict(db_session=db_session()))
    ], default_handler_class=NotFoundHandler, autoreload=is_dev_mode)


if __name__ == "__main__":
    app = make_app()
    app.listen(8888)
    tornado.ioloop.IOLoop.current().start()
